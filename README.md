# vagrant-ha-openshift
Openshift Vagrant playground to test High Availability features with GlusterFS

**Special thanks to Mans Matulewicz for coming up with cool ideas to improve this code, check out his stuff on Github.com**
https://github.com/MansM  

## Hints for setup in restricted environments

If you are doing the setup in an restricted network, e. g. corporate network and are forced to use a proxy or are not allowed to talk to an arbitrary DNS server, please refer to README.restricted.environment.md

**I highly recommend not to use these if you can get away with it...**

## DNS resolving(update)
For Mac users the vagrant plugin "landrush" works pretty good. But for others this might not be the case, see README.restricted.environment.md.
- You can try **nip.io** as an alternative wildcard DNS service:
- http://nip.io/

# Overview

# Vagrant
There 4 different vagrant-config files, the names explain what they do. You need to edit the main Vagrantfile with the proper config file name in this line:
```ruby
config_yml = YAML.load_file(File.open(File.expand_path(File.dirname(__FILE__)) + "/vagrant-config-3nodes-with-gluster.yml"))
```
In this case a 3 node Openshift cluster with 3 node Gluster cluster and 1 registry server will be build. I will use this setup in the rest of the following short how-to.

# Openshift
The inventories directory has matching Openshift installer inventories.

# Testing with Ansible Vault
### Update
**I've removed the ansible-vault encryption for the sake of faster deployment testing. (not typing a password every time)**

Encrypt secrets in the inventory.
To check or edit the encrypted vaul-vars.yml file: (The password is "test1234")
```sh
ansible-vault edit inventory-simple/vault-vars.yml
```
This will open your default shell editor (export EDITOR=vim)
```yaml
# These are the vault encrypted variables
# Set variables common for all OSEv3 hosts
[OSEv3:vars]

openshift_master_htpasswd_users={'admin': '$apr1$6CZ4noKr$IksMFMgsW5e5FL0ioBhkk/', 'developer': '$apr1$AvisAPTG$xrVnJ/J0a83hAYlZcxHVf1'}
```
You can add or remove variables to this file and be secure! Just remember to remove them from the plain-text inventory first ;-)

# Temp fixes:
These bits will have to be solved but for now I don't see an other solution.
Gluster Heketi needs a SSH key to connect.
- Copy the heketi_key to /tmp on your laptop: (because the inventory file needs a full path...on

```sh
cp provision/keys/heketi_key /tmp/
```

## For testing it's easier to deploy in three stages: (or use the bash script after cloning openshift-ansible)
- First clone the OpenShift Ansible repository

```sh
git clone https://github.com/openshift/openshift-ansible
cd openshift-ansible
git checkout release-3.9
git pull
cd ../
```
 - (at this point you can also run the simple build-vagrant-config-3nodes-with-gluster.sh script and wait till it's done)

```
- Vagrant 

```sh
vagrant up
```
- Create snaphost (optional)

```sh
vagrant status |grep running |cut -d" " -f1 |while read I;do vagrant snapshot save $I $I-snap;done
```

- Now deploy the OpenShift prerequisites

```sh
ansible-playbook -i inventories/3nodes-with-gluster/ openshift-ansible/playbooks/prerequisites.yml
```
- Next deploy OpenShift

```sh
ansible-playbook -i inventories/3nodes-with-gluster/ openshift-ansible/playbooks/deploy_cluster.yml
```
- You can do a uninstall of Openshift like this:

```sh
ansible-playbook -i inventories/3nodes-with-gluster/ openshift-ansible/playbooks/adhoc/uninstall.yml
```
- You can do a uninstall of just Heketi like this:

```sh
ansible-playbook -i inventories/3nodes-with-gluster/ openshift-ansible/playbooks/openshift-glusterfs/uninstall.yml
```

- Redeploy deploy Heketi

```sh
ansible-playbook -i inventories/3nodes-with-gluster/ openshift-ansible/playbooks/openshift-glusterfs/config.yml
```
- To destroy all use vagrant as shown below:

```sh
vagrant destroy -f
vagrant landrush rm --all
```

# When the install is done you can connect to the Openshift console with the following link:
- https://console-openshift-cluster.vagrant.test:8443/

# The passwords
```sh
admin adm-password
developer devel-password
```
or login to the osmaster and setup "oc" like this:
```sh
vagrant ssh osmaster1
[vagrant@osmaster1 ~]$ oc projects
You have access to the following projects and can switch between them with 'oc project <projectname>':

    * default
      glusterfs
      kube-public
      kube-system
      logging
      management-infra
      openshift
      openshift-infra
      openshift-metrics-node-exporter
      openshift-node
      openshift-web-console

Using project "default" on server "https://console-openshift-cluster.vagrant.test:8443".
```
# Troubleshooting
If you run the cluster a few days and things start to get weird try this:
```sh
vagrant reload
```
# Contribute
Feel free to create issues!
