# -*- mode: ruby -*-
# vi: set ft=ruby :

if ARGV.length == 2
  if ARGV[1] == '-f'
    puts "You must use 'vagrant #{ARGV[0]} <name>, [<name>] ...'."
    puts "Run 'vagrant status' to view VM names."
    exit 1
  end
end

require 'yaml'
config_yml = YAML.load_file(File.join(__dir__, 'vagrant-config-3nodes-with-gluster.yml'))

Vagrant.configure(2) do |config|
  # Load plugins
  if Vagrant.has_plugin?('landrush')
    config.landrush.enabled = true
    config.landrush.host 'console-openshift-cluster.vagrant.test',
                         'osmaster1.vagrant.test'
    config.landrush.host 'apps.openshift-cluster.vagrant.test',
                         'osmaster1.vagrant.test'
  else
    abort('Please install the landrush plugin, vagrant plugin install landrush')
  end

  config_yml[:vms].each do |name, settings|
    config.vm.define name  do |vm_config|
      vm_config.vm.box = 'scorputty/centos'
      config.vm.provider 'virtualbox' do |v|
        v.linked_clone = true
      end
      vm_config.vm.hostname = settings[:hostname]
      vm_config.vm.network 'private_network', ip: settings[:ip]
      vm_config.vm.synced_folder './', '/vagrant'
      vm_config.ssh.insert_key = false
      vm_config.vm.provider 'virtualbox' do |vb|
        vb.name = settings[:hostname]
        vb.cpus = settings[:cpu]
        vb.memory = settings[:memory]
        if settings.key?(:dsize)
          disk_name = "#{name}_disk.vdi"
          disk_size = settings[:dsize]
          controller_name = 'IDE Controller'
          unless File.exist?(disk_name)
            vb.customize ['createmedium',
                          '--filename',
                          disk_name,
                          '--size', 1024 * disk_size]
          end
          vb.customize ['storageattach',
                        :id, '--storagectl',
                        controller_name,
                        '--port', 1,
                        '--device', 0,
                        '--type',
                        'hdd',
                        '--medium', disk_name]
        end
      end

      if (settings[:hostname]).to_s == config_yml[:vms][config_yml[:vms].keys.last][:hostname]
        vm_config.vm.provision :ansible do |ansible|
          ansible.playbook = 'provision/site.yml'
          ansible.groups = config_yml[:groups]
          ansible.verbose = 'vv'
          ansible.host_key_checking = false
          ansible.limit = 'all'
          ansible.extra_vars = {
            deploy_env: 'vagrant',
            openshift_image_tag: 'v3.9.0'
          }
        end
      end
    end
  end
end
