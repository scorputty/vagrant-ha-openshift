#!/usr/bin/env bash

# Super lazy bash script to build the whole thing in one go (except for the gitclone)

cp provision/keys/heketi_key /tmp/

time vagrant up && \

# Taking snapshots is optional
# vagrant status |grep running |cut -d" " -f1 |while read I;do vagrant snapshot save $I $I-snap;done && \

ansible-playbook -i inventories/3nodes-with-gluster/ openshift-ansible/playbooks/prerequisites.yml && \

ansible-playbook -i inventories/3nodes-with-gluster/ openshift-ansible/playbooks/deploy_cluster.yml
