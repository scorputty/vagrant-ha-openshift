# Hints for setup in restricted environments

If you are doing the setup in an restricted network, e. g. corporate network here are some hints for you.

**I highly recommend not to use these if you can get away with it...**

## You are forced to use a Proxy
If you need to use a proxy and want to test how this effects a OpenShift setup and install you can do that with this test setup.

### Vagrantfile proxy section:
```ruby
# Comment out if no Proxy is needed
if Vagrant.has_plugin?("vagrant-proxyconf")
  config.proxy.http     = "http://10.0.2.2:3128"
  config.proxy.https    = "http://10.0.2.2:3128"
  config.proxy.no_proxy = "localhost,127.0.0.1,.vagrant.test,kube-service-catalog.svc,192.168.42.0/24,172.30.0.0/16,10.128.0.0/14"
end
```

### OpenShift inventory proxy section (hosts.yml)
```ruby
# Comment out if no Proxy is needed
openshift_http_proxy=http://10.0.2.2:3128
openshift_https_proxy=http://10.0.2.2:3128
openshift_no_proxy="localhost,127.0.0.1,.vagrant.test,kube-service-catalog.svc,192.168.42.22,192.168.42.32,192.168.42.33,192.168.42.52,192.168.42.53,172.30.0.0/16,10.128.0.0/14"
```

## Specific DNS Server

If you restricted to use some specific DNS servers only, there are things that can help.

### Specific DNS Server for landrush plugin

Landrush provides a DNS service for your vagrant VMs. Queries for external names it is unable to answer itself it forwrds to some upstream resolver, by default it uses the Google DNS servers. But you can provide some other by setting `config.landrush.upstream`. You can use systemd-resolved stub resolver (which is usually dynamically updated by network manager) like shown below or directly enter the IP of your DNS server.

Best location to set is `~/.vagrant.d/Vagrantfile`. Settings you are doing there are included always you use vagrant. So it helps for other Vagrantfiles too and you do not need to modify each projects Vagrantfile separately.


```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  if Vagrant.has_plugin?("landrush")
    # set upstream DNS server for external DNS queries
    # by default landrush uses Google DNS, but arbitray DNS is not allowed
    # in every network

    # 127.0.0.53 is the systemd-resolved stub resolver.
    # run "systemd-resolve --status" to see details about the actual nameservers.

    config.landrush.upstream '127.0.0.53'
  end
end
```
